import { shallowMount } from '@vue/test-utils'
import Autocomplete from '@/components/Autocomplete.vue'

import {
  getCities
} from '@/lib/cities'

jest.mock('@/lib/cities', () => ({
  getCities: jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve({
        data: []
      }),
    })
  )
}))

describe('Autocomplete.vue', () => {
  let vm, wrapper
  beforeEach(() => {
    getCities.mockClear()
    wrapper = shallowMount(Autocomplete)
    vm = wrapper.vm
  })
  it('Comprobar estado inicial', () => {
    expect(vm.state.searchField).toBe('')
    expect(vm.state.noResults).toBe(false)
    expect(vm.state.searching).toBe(false)
  })

  it('Comprobar llamada al fetch', () => {
    wrapper.find('#search').trigger('keyup')
    expect(getCities).toHaveBeenCalledTimes(0)

    vm.state.searchField = 'Mad'
    wrapper.find('#search').trigger('keyup')
    expect(getCities).toHaveBeenCalledWith('mad')
  })
})
