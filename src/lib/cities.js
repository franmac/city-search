/**
 * @param queryString
 * @returns {Promise.<void>}
 */
// Devuelve las ciudades que contienen la cadena pasada como parámetro
const getCities = (queryString) => {
  return fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${queryString}`, {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": process.env.VUE_APP_API_KEY,
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com"
    }
  })
}

export { getCities }